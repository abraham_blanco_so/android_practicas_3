package com.sitiouno.myapplication.presentador;

public interface IRecyclerViewFragmentPresenter {

    public void obtenerContactosBaseDatos();

    public void mostrarContactosRV();
}
