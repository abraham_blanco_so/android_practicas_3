package com.sitiouno.myapplication.fragment;

import com.sitiouno.myapplication.adapter.ContactoAdaptador;
import com.sitiouno.myapplication.pojo.Contacto;

import java.util.ArrayList;

public interface IRecyclerViewFragmentView {

    public void generarLinearLayoutVertical();

    public ContactoAdaptador crearAdaptador(ArrayList<Contacto> contactos);

    public void inicializarAdaptadorRV(ContactoAdaptador adaptador);
}
