package com.sitiouno.myapplication.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.sitiouno.myapplication.R;
import com.sitiouno.myapplication.pojo.Contacto;

import java.util.ArrayList;

public class ConstructorContactos {

    private static final int LIKE = 1;
    private Context context;
    public ConstructorContactos(Context context){
        this.context = context;
    }

    public ArrayList<Contacto> obtenerDatos(){
        /*ArrayList<Contacto> contactos = new ArrayList<>();
        contactos.add(new Contacto(R.drawable.lollipop_9,"Anahi Salgado", "123456789", "anahi@gmail.com", 5));
        contactos.add(new Contacto(R.drawable.banana_9,"Abraham Blanco", "22222444444", "abrham@gmail.com", 3));
        contactos.add(new Contacto(R.drawable.mushrooms,"lorenzo Payaro", "333333555555", "lorenxo@gmail.com", 1));
        contactos.add(new Contacto(R.drawable.garfio_9,"Mi niño", "444444555555", "mediamano@gmail.com", 4));
        return contactos;*/

        BaseDatos db = new BaseDatos(context);
        insertarTresContactos(db);
        return db.obtenerTodosLosContactos();
    }

    public void insertarTresContactos(BaseDatos db){
        ContentValues contentValues = new ContentValues();

        //0
        contentValues.put(ConstantesBaseDatos.TABLE_CONTACTS_NOMBRE,"Larry Capinga");
        contentValues.put(ConstantesBaseDatos.TABLE_CONTACTS_TELEFONO,"666666666");
        contentValues.put(ConstantesBaseDatos.TABLE_CONTACTS_EMAIL,"larrycapinga@gmail.com");
        contentValues.put(ConstantesBaseDatos.TABLE_CONTACTS_FOTO,R.drawable.banana);

        db.insertarContacto(contentValues);

        //1
        contentValues = new ContentValues();

        contentValues.put(ConstantesBaseDatos.TABLE_CONTACTS_NOMBRE,"Alan Brito");
        contentValues.put(ConstantesBaseDatos.TABLE_CONTACTS_TELEFONO,"666666666");
        contentValues.put(ConstantesBaseDatos.TABLE_CONTACTS_EMAIL,"alanbrito@gmail.com");
        contentValues.put(ConstantesBaseDatos.TABLE_CONTACTS_FOTO,R.drawable.lollipop);

        db.insertarContacto(contentValues);

        //2
        contentValues = new ContentValues();

        contentValues.put(ConstantesBaseDatos.TABLE_CONTACTS_NOMBRE,"Ali Cate");
        contentValues.put(ConstantesBaseDatos.TABLE_CONTACTS_TELEFONO,"666666666");
        contentValues.put(ConstantesBaseDatos.TABLE_CONTACTS_EMAIL,"alicate@gmail.com");
        contentValues.put(ConstantesBaseDatos.TABLE_CONTACTS_FOTO,R.drawable.mushrooms);

        db.insertarContacto(contentValues);
    }

    public void darLikeContacto(Contacto contacto){
        BaseDatos db = new BaseDatos(context);
        ContentValues contentValues = new ContentValues();

        contentValues.put(ConstantesBaseDatos.TABLE_LIKES_CONTACT_ID_CONTACTO,contacto.getId());
        contentValues.put(ConstantesBaseDatos.TABLE_LIKES_CONTACT_NUMERO_LIKES, LIKE);

        db.insertarLikeContacto(contentValues);
    }

    public int obtenerLikesContacto(Contacto contacto){

        BaseDatos db = new BaseDatos(context);
        return db.obtenerLikesContacto(contacto);
    }
}
